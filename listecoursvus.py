import pandas as pd
import dash
from dash import html, dash_table
from api import get_course_lectures

app = dash.Dash(__name__)

cours_les_plus_vus = pd.DataFrame(get_course_lectures())

app.layout = html.Div([
    html.H4('Liste des 30 cours les plus vus'),
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col} for col in cours_les_plus_vus.columns],
        data=cours_les_plus_vus.to_dict('records'),
        style_cell=dict(textAlign='left'),
        style_header={'backgroundColor': 'paleturquoise', 'fontWeight': 'bold'},
        style_data={'backgroundColor': 'lavender'},
              ), 
])

app.run_server(debug=True)