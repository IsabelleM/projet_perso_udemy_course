import pandas as pd
import mysql.connector
from datetime import datetime
from login import monlogin2

# Lecture des fichiers csv en dataframe
df1 = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/udemy_courses.csv")

df2 = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/courses.csv")

df2.columns = ['id','title','url','rating','num_reviews','num_lectures','created','last_update_date',
               'duration','instructors_id','image']

df1.columns = ['id','title','url','is_paid','price','num_subscribers','num_reviews','num_lectures',
               'level','duration','published_timestamp','subject']

# Création d'une colonne date pour chaque dataframe
df2['date'] = pd.to_datetime(df2['created'])
df1['date'] = pd.to_datetime(df1['published_timestamp'])

# Suppression de colonnes
df1 = df1.drop(['published_timestamp', 'url'], axis = 1)
df2 = df2.drop(['image', 'created', 'url'], axis = 1)

# Simplication et harmonisation de la colonne duration
df2["duration"] = df2["duration"].replace("total hours", '', regex=True)
df2["duration"] = df2["duration"].replace("total hour", '', regex=True)
df2["duration"] = df2["duration"].replace("questions", '', regex=True)
df2["duration"] = df2["duration"].replace("total mins", '', regex=True)
df2['duration'] = df2['duration'].astype(float)

# Merge des deux dataframes
df_merge = pd.merge(df1, df2, how='outer', on=["id", "title", "num_reviews", "num_lectures", "duration", "date"])
# Suppresion des valeurs dupliquées 
df_merge.drop_duplicates(inplace=True)
df_merge

# Enlever les expressions régulières
df_merge['title'] = df_merge['title'].replace(to_replace=r'[^\w\s]', value='', regex=True)
# Mettre la colonne titre du cours en minuscules
df_merge['title'] = df_merge['title'].str.lower()
# Enlever les espaces avant et après dans la colonne titre
df_merge['title'] = df_merge['title'].str.strip()

# Remplacer les valeurs NaN 
df_merge['rating'].fillna(0, inplace=True)
df_merge['num_subscribers'].fillna(0, inplace=True)
df_merge['num_reviews'].fillna(0, inplace=True)
df_merge['price'].fillna(0, inplace=True)

# Remplacer les valeurs NaN dans la colonne 'date' par la date actuelle
df_merge['last_update_date'].fillna(datetime.now().date(), inplace=True)
df_merge['level'].fillna('All Levels', inplace=True)
df_merge['subject'].fillna('Web Development', inplace = True)

# Conversion des types après remplissage des NaN
df_merge['num_subscribers'] = df_merge['num_subscribers'].astype(int)
df_merge['num_reviews'] = df_merge['num_reviews'].astype(int)
df_merge['price'] = df_merge['price'].astype(int)
# Remplacer les float par des int
df_merge['instructors_id'] = pd.to_numeric(df_merge['instructors_id'], errors='coerce').fillna(0).astype(int)

# Remplacer les valeurs NaN dans la colonne 'is_paid' par True
df_merge['is_paid'].fillna(True, inplace=True)


# Lecture du dataframe des inscrits
df_inscrits = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/listeinscrits.csv")


# Lecture et nettoyage du dataframe instructeurs
df3 = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/instructors.csv")

# Suppression de colonnes
df3 = df3.drop(['_class', 'image_50x50', 'image_100x100', 'initials', 'url'], axis = 1)

# Mettre la colonne titre du cours en minuscules
df3['job_title'] = df3['job_title'].str.lower()
# Enlever les espaces avant et après dans la colonne titre
df3['job_title'] = df3['job_title'].str.strip()
df3['title'] = df3['title'].str.strip()
df3['display_name'] = df3['display_name'].str.strip()

# Enlever les expressions régulières
df3['title'] = df3['title'].replace(to_replace=r'[^\w\s]', value='', regex=True)
df3['job_title'] = df3['job_title'].replace(to_replace=r'[^\w\s]', value='', regex=True)

# Remplacer les valeurs NaN
df3['name'].fillna('', inplace=True)
df3['job_title'].fillna('', inplace=True)
df3['id'] = df3['id'].astype(int)



# Connection à la base de données
connection = mysql.connector.connect(**monlogin2)

cursor = connection.cursor()

# Insertion des instructeurs
for index, row in df3.iterrows():
    cursor.execute("""INSERT IGNORE INTO instructors (instructors_id, title, name, display_name, job_title) 
                   VALUES (%s, %s, %s, %s, %s)""", (row['id'], row['title'], row['name'], row['display_name'], row['job_title']))
    connection.commit()

    
# Insertion des cours dans la table 'cours'
for index, row in df_merge.iterrows():
    if row['instructors_id'] == 0:
        iid = None
    else:
        iid = row['instructors_id']
    
    cursor.execute("""INSERT IGNORE INTO cours (cours_id, title, rating, 
                       num_subscribers, num_reviews, num_lectures, date, 
                       last_update_date, duration, level, subject, is_paid, 
                       price, instructors_id)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                        (
                            row['id'], row['title'], row['rating'], row['num_subscribers'], 
                            row['num_reviews'], row['num_lectures'], row['date'], 
                            row['last_update_date'], row['duration'], row['level'], row['subject'], row['is_paid'], 
                            row['price'], iid))

    connection.commit()

# Insertion des inscrits dans la table 'inscrits'
for index, row in df_inscrits.iterrows():
    cursor.execute("""INSERT INTO inscrits (inscrits_id, first_name, last_name, country) VALUES (%s, %s, %s, %s)""", 
                      (row['id'], row['first_name'], row['last_name'], row['country']))
    connection.commit()

# Insertion des associations cours-inscrits dans la table 'cours_inscrits'
for index, row in df_inscrits.iterrows():
    inscrits_id = row['id']
    cours_id = row['cours_id']
  # Insertion dans la table 'cours_inscrits'
    cursor.execute("""INSERT INTO cours_inscrits (cours_id, inscrits_id, avancement) 
                        VALUES (%s, %s, %s)""",
                        (cours_id, inscrits_id, row['avancement']))
    connection.commit()
        

# Fermeture du curseur et de la connection
cursor = connection.cursor()
connection.close()