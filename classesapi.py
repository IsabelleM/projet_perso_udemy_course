from pydantic import BaseModel

class Cours_souscrits(BaseModel):
    title: str
    num_subscribers: int
    subject: str

class Cours_les_plus_vus(BaseModel):
    title: str
    num_lectures: int
    subject: str

class Nombre_d_inscrits(BaseModel):
    nombre_inscrits: int
    country: str

class Instructors(BaseModel):
    title: str
    instructors_id: int

class Cours_populaires(BaseModel):
    cours: str
    rating: int
    instructor: str
    nombre_vus: int

class NombreInscrits(BaseModel):
    nombre_inscrits: int
    level: str