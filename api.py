from fastapi import FastAPI
from typing import List, Dict
from pydantic import BaseModel
from classesapi import Cours_souscrits, Cours_les_plus_vus, Nombre_d_inscrits, Instructors, Cours_populaires, NombreInscrits
import mysql.connector
from login import monlogin2


app = FastAPI()

mydb = mysql.connector.connect(**monlogin2)


# Afficher les 30 cours les plus souscrits
@app.get("/cours_souscrits", response_model=List[Cours_souscrits]) 
def get_course_subscribers():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT title, num_subscribers, subject FROM cours 
        ORDER BY num_subscribers DESC LIMIT 30;
        """)
    return cursor.fetchall()


# Afficher les 30 premiers cours les plus vus
@app.get("/cours_les_plus_vus", response_model=List[Cours_les_plus_vus])
def get_course_lectures():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT title, num_lectures, subject FROM cours 
        ORDER BY num_lectures DESC, subject LIMIT 30;
        """)
    return cursor.fetchall()


# Obtenir le nombre d'inscrits par sujet et par niveau
@app.get("/cours/{subject}", response_model=List[NombreInscrits])
def get_number_subscribers(subject:str):
    subject = subject.replace("_", " ")
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(f""" 
        SELECT SUM(num_subscribers) AS nombre_inscrits, level FROM cours WHERE subject LIKE '%{subject}%' 
        GROUP BY subject, level;
        """)
    return cursor.fetchall()


# Afficher le nombre d'inscrits par pays
@app.get("/inscrits/{country}", response_model=Nombre_d_inscrits)
def get_number_subscribers_pays(country:str):
    cursor = mydb.cursor(dictionary=True)
    sql = """ 
       SELECT count(*) as nombre_inscrits, country FROM inscrits 
       WHERE country = %s GROUP BY country ORDER BY nombre_inscrits DESC;"""
    cursor.execute(sql, (country,))
    return cursor.fetchall()[0]


# Afficher les cours par nom de l'instructeur
@app.get("/instructors/{display_name}", response_model=List[Instructors])
def get_course_title_by_instructor(display_name: str):
    display_name = display_name.replace("_", " ")
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT cours.title, instructors.instructors_id FROM cours 
        JOIN instructors ON instructors.instructors_id = cours.instructors_id 
        WHERE instructors.display_name = %s;
        """, (display_name,))
    return cursor.fetchall()


# Ajouter un cours
@app.post("/cours")
def insert_cours(cours_id: int, title, rating, num_subscribers, num_reviews, num_lectures,
                date, last_update_date, duration, level, subject, is_paid, price, instructors_id):
    cursor = mydb.cursor()
    cursor.execute("""INSERT INTO cours(cours_id, title, rating, num_subscribers, num_reviews, 
                   num_lectures, date, last_udate_date, duration, level, subject, is_paid, price, 
                   instructors_id)
                   VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", 
                   [cours_id, title, rating, num_subscribers, num_reviews, num_lectures,
                    date, last_update_date, duration, level, subject, is_paid, price, instructors_id])
    mydb.commit()
    return "cours inséré avec succès"


# Afficher les cours les plus populaires
@app.get("/cours_populaires", response_model=List[Cours_populaires])
def cours_les_plus_populaires():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute("""SELECT cours.title as cours, rating, instructors.title as instructor, MAX(num_lectures) AS nombre_vus 
                    FROM cours 
                    JOIN instructors ON instructors.instructors_id = cours.instructors_id 
                    WHERE rating = 5 
                    GROUP BY cours.title, rating, instructors.title 
                    ORDER BY MAX(num_lectures) DESC LIMIT 10;
                    """)
    return cursor.fetchall()
