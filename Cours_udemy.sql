DROP DATABASE IF EXISTS cours_udemy;

CREATE DATABASE cours_udemy;

USE cours_udemy;

CREATE TABLE instructors (
  instructors_id INT PRIMARY KEY,
  title varchar(255),
  name varchar(255),
  display_name varchar(255),
  job_title varchar(255)
);

CREATE TABLE cours (
  cours_id INT PRIMARY KEY NOT NULL,
  title VARCHAR(255),
  rating float,
  num_subscribers int,
  num_reviews int,
  num_lectures int,
  date date,
  last_update_date date,
  duration float,
  level ENUM("All Levels", "Beginner Level", "Intermediate Level", "Expert Level"),
  subject ENUM("Business Finance", "Graphic Design", "Musical Instruments", "Web Development"),
  is_paid bool,
  price int,
  instructors_id int,
  FOREIGN KEY (instructors_id) REFERENCES instructors(instructors_id)
);

CREATE TABLE inscrits (
  inscrits_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  first_name varchar(56),
  last_name varchar(56),
  country varchar(56)
);

CREATE TABLE cours_inscrits (
  cours_id INT NOT NULL,
  inscrits_id INT NOT NULL,
  avancement INT,
  FOREIGN KEY (cours_id) REFERENCES cours(cours_id),
  FOREIGN KEY (inscrits_id) REFERENCES inscrits(inscrits_id)
);



