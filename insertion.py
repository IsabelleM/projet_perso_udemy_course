import pandas as pd
import mysql.connector
from login import monlogin

df = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/udemy_courses.csv")

#Ajouter une colonne date à partir de la colonne published_timestamp
df['date'] = pd.to_datetime(df['published_timestamp'])

#Suppression de deux colonnes
df = df.drop(['published_timestamp', 'url'], axis=1)

# Enlever les expressions régulières
df['course_title'] = df['course_title'].replace(to_replace=r'[^\w\s]', value='', regex=True)

# Mettre la colonne titre du cours en minuscules et enlever les espaces avant et après
df['course_title'] = df['course_title'].str.lower()
df['course_title'] = df['course_title'].str.strip()
print(df.head())

#Imprime les valeurs dupliquées dans le dataframe
print(df[df.duplicated('course_id', keep=False)])

# Création du second dataframe
df_inscrits = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/listeinscrits.csv")


connection = mysql.connector.connect(**monlogin)



cursor = connection.cursor()

# Insertion des cours dans la table 'cours'
for index, row in df.iterrows():
    cursor.execute("""INSERT IGNORE INTO cours (course_id, course_title, content_duration, level, 
                    subject, is_paid, price, date, num_subscribers, num_reviews, num_lectures)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                    (row['course_id'], row['course_title'], row['content_duration'], row['level'], 
                     row['subject'], row['is_paid'], row['price'], row['date'], row['num_subscribers'], 
                     row['num_reviews'], row['num_lectures']))
    connection.commit()

# Insertion des inscrits dans la table 'inscrits'
for index, row in df_inscrits.iterrows():
    cursor.execute("""INSERT INTO inscrits (inscrits_id, first_name, last_name, country) 
                   VALUES (%s, %s, %s, %s)""", 
                    (row['id'], row['first_name'], row['last_name'], row['country']))
    connection.commit()

# Insertion des associations cours-inscrits dans la table 'cours_inscrits'
for index, row in df_inscrits.iterrows():
    inscrits_id = int(row['id'])
    course_id = int(row['course_id'])

    # Vérifier si course_id existe dans la table cours
    cursor.execute("SELECT course_id FROM cours WHERE course_id = %s", (course_id,))
    result = cursor.fetchone()
    
    if result:
        # Insertion dans la table cours_inscrits si course_id existe
        cursor.execute("""INSERT INTO cours_inscrits (course_id, inscrits_id, avancement) 
                         VALUES (%s, %s, %s)""", 
                       (course_id, inscrits_id, row['avancement']))
        connection.commit()
    else:
        print(f"Le course_id {course_id} n'existe pas dans la table cours. L'inscription n'a pas été ajoutée.")
   

# Fermeture du curseur et de la connection
cursor = connection.cursor()
connection.close()
