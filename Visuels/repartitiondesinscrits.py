from dash import dash, Dash, dcc, html
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go


df = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/udemy_courses.csv")

#Ajouter une colonne date à partir de la colonne published_timestamp
df['date'] = pd.to_datetime(df['published_timestamp'])

#Suppression de deux colonnes
df = df.drop(['published_timestamp', 'url'], axis=1)

# Enlever les expressions régulières
df['course_title'] = df['course_title'].replace(to_replace=r'[^\w\s]', value='', regex=True)

# Mettre la colonne titre du cours en minuscules et enlever les espaces avant et après
df['course_title'] = df['course_title'].str.lower()
df['course_title'] = df['course_title'].str.strip()

# Ajouter une colonne année à partir de la colonne date
df['year']= df['date'].dt.year
df1 = df.groupby('year')[['num_subscribers','num_reviews','num_lectures']].sum().reset_index()


app = Dash(__name__)

fig = px.line(df1, x='year', y=['num_subscribers','num_reviews','num_lectures'], title="Répartition des inscrits, avis et lectures")
fig.show()

app = Dash()
app.layout = html.Div([
    dcc.Graph(figure=fig)
])

app.run_server(debug=True, use_reloader=False) 