from dash import Dash, dcc, html
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.graph_objects as go


df = pd.read_csv("/home/isabelle/projet_perso_udemy_course/projet_perso_udemy_course/udemy_courses.csv")

#Ajouter une colonne date à partir de la colonne published_timestamp
df['date'] = pd.to_datetime(df['published_timestamp'])

#Suppression de deux colonnes
df = df.drop(['published_timestamp', 'url'], axis=1)

# Enlever les expressions régulières
df['course_title'] = df['course_title'].replace(to_replace=r'[^\w\s]', value='', regex=True)

# Mettre la colonne titre du cours en minuscules et enlever les espaces avant et après
df['course_title'] = df['course_title'].str.lower()
df['course_title'] = df['course_title'].str.strip()


nombre_de_cours = np.round(df['subject'].value_counts(normalize=True)*100,2)

app = Dash(__name__)

# Pie chart représentant la répartition des cours en pourcentage
labels = nombre_de_cours.index  
values = nombre_de_cours.values 

fig = go.Figure(data=[go.Pie(labels=labels, values=values)])

# Ajouter un titre au diagramme
fig.update_layout(title="Répartition des cours par sujet en pourcentage")


app = Dash()

app.layout = html.Div([
    dcc.Graph(figure=fig)
])

app.run_server(debug=True, use_reloader=False) 