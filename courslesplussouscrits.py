import pandas as pd
import dash
from dash import html, dash_table
from dash.dependencies import Input, Output
from api import get_course_subscribers

app = dash.Dash(__name__)

cours_les_plus_souscrits = pd.DataFrame(get_course_subscribers())

app.layout = html.Div([
    html.H4('Liste des 30 cours les plus souscrits'),
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col} for col in cours_les_plus_souscrits.columns],
        data=cours_les_plus_souscrits.to_dict('records'),
        style_cell=dict(textAlign='left'),
        style_header={'backgroundColor': 'paleturquoise', 'fontWeight': 'bold'},
        style_data={'backgroundColor': 'lavender'}
        ), 
        html.Div(id='table_out')
])

@app.callback(
    Output('table_out', 'children'), 
    Input('table', 'active_cell'))
def update_output(active_cell):
    if active_cell:
        cell_data = cours_les_plus_souscrits.iloc[active_cell['row']][active_cell['column_id']]
        return f"Data: \"{cell_data}\" from table cell: {active_cell}"
    return "Click the table"

if __name__ == '__main__':
    app.run_server(debug=True)