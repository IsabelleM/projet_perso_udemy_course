# Cours Udemy
Ce projet a pour but de faire une analyse des cours udemy d'après un dataset des cours udemy de 2010 à 2017 créé par Chase Wilden.

Il faut utiliser un environnement virtuel appelé env. 
python3 -m venv env
source ./.venv/bin/activate
Et voir le fichier requirements.txt pour avoir le numéo des différentes versions.

# Login
Pour se loguer à la base de données, il faut fournir dans un fichier qui s'appelle login.py, à la racine du projet, et contenant les variables suivantes:
monlogin = {"host":"", "user":"","password":"","database":"udemy_course"}
et
monlogin2 = {"host":"", "user":"","password":"","database":"cours_udemy"}

# Fichiers sources
Les cinq fichiers sources utilisés sont:
- udemy_courses.csv pour le premier modèle
- listeinscrits.csv et cours_inscrits.csv pour le deuxième modèle
- courses.csv et instructors.csv pour le troisième modèle

# API
Pour ouvrir l'API, il faut taper: uvicorn api:app --reload

