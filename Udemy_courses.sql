DROP DATABASE IF EXISTS udemy_course;

CREATE DATABASE udemy_course;

USE udemy_course;

CREATE TABLE cours (
  course_id INT PRIMARY KEY NOT NULL,
  course_title text,
  content_duration float,
  level ENUM("All Levels", "Beginner Level", "Intermediate Level", "Expert Level"),
  subject ENUM("Business Finance", "Graphic Design", "Musical Instruments", "Web Development"),
  is_paid bool,
  price INT,
  date date,
  num_subscribers INT,
  num_reviews INT,
  num_lectures INT
);

CREATE TABLE inscrits (
  inscrits_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  first_name varchar(56),
  last_name varchar(56),
  country varchar(56)
);

CREATE TABLE cours_inscrits (
  course_id INT NOT NULL,
  inscrits_id INT NOT NULL,
  avancement INT,
  FOREIGN KEY (course_id) REFERENCES cours(course_id),
  FOREIGN KEY (inscrits_id) REFERENCES inscrits(inscrits_id)
);