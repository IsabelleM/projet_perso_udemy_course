import pandas as pd
import dash
from dash import html, dash_table, Dash
from dash.dependencies import Input, Output
from api import cours_les_plus_populaires

app = Dash(__name__)

cours_pop = pd.DataFrame(cours_les_plus_populaires())

app.layout = html.Div([
    html.H4('Liste des 10 premiers cours les plus populaires'),
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col} for col in cours_pop.columns],
        data=cours_pop.to_dict('records'),
        style_cell=dict(textAlign='left'),
        style_header={'backgroundColor': 'paleturquoise', 'fontWeight': 'bold'},
        style_data={'backgroundColor': 'lavender'}
    ), 
    html.Div(id='table_out') 
])


@app.callback(
    Output('table_out', 'children'), 
    Input('table', 'active_cell'))
def update_output(active_cell):
    if active_cell:
        cell_data = cours_pop.iloc[active_cell['row']][active_cell['column_id']]
        return f"Data: \"{cell_data}\" from table cell: {active_cell}"
    return "Click the table"

if __name__ == '__main__':
    app.run_server(debug=True)
