from dash import dash, Dash, dcc, html
from dash import dash_table
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import requests

def get_cours_instructors():
    response = requests.get('http://127.0.0.1:8000/instructors/Jose%20Portilla')
    if response.status_code == 200:
        a = response.json()
        return pd.DataFrame(a)
    else:
        return pd.DataFrame()

data = get_cours_instructors()

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H4('Liste des 30 cours les plus vus'),
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col} for col in data.columns],
        data=data.to_dict('records'),
        style_cell=dict(textAlign='left'),
        style_header={'backgroundColor': 'paleturquoise', 'fontWeight': 'bold'},
        style_data={'backgroundColor': 'lavender'},
    ), 
])

if __name__ == '__main__':
    app.run_server(debug=True)